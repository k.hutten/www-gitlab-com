---
layout: handbook-page-toc
title: "GitLab President's Club"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
Incentive trip to celebrate the successes of our top Sales Professionals and the supporting individuls that helped them get there. Honoring a year of hard work and dedicationt to achieving results and delivering value for the organization. 

## How to Qualify

| #  | CATEGORIES                                                    |
|----|---------------------------------------------------------------|
| 1  | Top 5 SALs Ranking by % of annual full quota                 |
| 2  | Top 4 MMs Ranking by % of annual full quota                  |
| 3  | Top 2 ENT / Top 1 COM ASM >6mo tenure, top quota performance |
| 4  | Top ENT RD >6mo tenure, 100%, top quota performance          |
| 5  | Top SMB by VP/CRO selection                                 |
| 6  | Top 4 SAs by VP/CRO selection                               |
| 7  | Top ISR >6mo by VP/CRO selection                            |
| 8  | Top Channel/Alliances >6mo tenure by VP/CRO selection       |
| 9  | Top 4 CS (TAM, PSE) by VP/CRO selection                     |
| 10 | Top 2 SDR by CMO selection                                  |
| 11 | eGroup & CRO Leaders                                          |
| 12 | MVPs (non-Sales) by CRO Staff selection                       |

## FAQ
Can I bring a guest?
 * Each person who qualifies is able to bring one guest over 21. We cannot acomodate any guests beyond this and cannot allow any children at this event. 

How do I book travel?

What can I expense and what is Gitlab covering?

Do I need to register?

I still have questions... email salesevents@gitlab.com
