---
layout: handbook-page-toc
title: The GitLab Procurement Team
---

## On this page
{:.no_toc}

- TOC
{:toc}


# The GitLab Procurement Team 

## Procurement KPI

Deliver quantified savings of > $250,000 each quarter.

Cost savings are achieved through the procure to pay process.
Savings are calculated as the savings achieved by comparing the initial vendor proposal price to the final purchase price.
In the event of a contract renewal, in addition to the above, savings can also be calculated as the savings achieved by comparing the previous cost per unit (eg. user, business metric, etc.) to the final cost per unit.
Savings negotiated at the cost per unit level are also cost savings to be calculated as savings.
Note these savings are not directly tied to budget.

Aligns with the following core business objectives:

* Control spend and build a culture of long-term savings on procurement costs.
* Streamline the purchasing process.
* Minimize financial risk.

## Requesting Procurement Services
 
There are several third-party vendor processes that procurement supports.
Team members can purchase goods and services on behalf of the company in accordance with the Signature Authorization Matrix and guide to Spending Company Money.
However, any purchases requiring contracts must first be reviewed by procurement, then signed off by a member of the executive team.
Be sure to also check out our guide on Signing Legal Documents.

### 1. New Vendor Evaluation

If you have a new business need and are seeking a third party vendor to support it, you must review the market capabilities defined by your overall spend BEFORE selecting your vendor.
Create a Vendor Contract Approval [Issue](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts) to begin the process.
Unless, however, you have a field marketing vendor contract that do NOT involve data processing such as marketing programs, sponsorships, hotels, and professional services that do not require processing of data.
In that case, create a Non-Data Vendor Contract Approval [Issue ](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts_marketing_events.md)

**Note: Before sharing details and/or confidential information regarding our business needs, please obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing) from the potential vendor(s).**

All vendors must adhere to the [GitLab Partner Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics).
Please inform your vendor it is mandatory they contractually adhere to this if they would like to do business with us.
(Note these are typically not required in marketing events agreements unless the vendor is providing services).

| Estimated Spend | Market Review Needed |
| ------ | ------ |
| <$25K | None |
| >$25K and <$50K | Two Bids | 
| >$50K and <$100K | Three Bids | 
| >$100K | RFP |

If there is a business reason why the market review is not necessary, please obtain a bid waiver from your functional leader and send to procurement for approval.
This can be done within the vendor contract [Issue ](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts)


### 2. Existing Vendor Negotiation (for Renewals and True-Ups)

If you have an existing vendor that needs a contract renewal or a true-up mid contract, work with procurement BEFORE agreeing to business terms.
For all contract renewals, **contact procurement 90-60 days before the existing contract expires** by opening an [Issue Template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts).
Procurement will engage our software buying partner Vendr to ensure that the quoted pricing is competitive.
Vendr can help negotiate directly with vendors on both new subscriptions and renewals.
It is preferred that we negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term relationships with vendors.

For instructions on how to submit an existing vendor negotiation, please see the LINK COMING SOON!

### 3. Vendor Contract Review

BEFORE agreeing to business terms, identify whether you need a New Vendor Market Review (No. 1 above), or an Existing Vendor Negotiation (No. 2 above).
If you have completed one of those, you are ready to seek review of a vendor contract.
Your request must go through GitLab's Vendor Management process to make sure it has been reviewed by appropriate stakeholders and that the request receives all necessary approvals.
This process involves Finance (for budgetary purposes), Security (when personal data is involved to ensure the vendor's systems and procedures meet our minimum standard), Business Operations, Procurement (for contract review), and an approved Signer (for final approval and signing).

For instructions on how to submit a new vendor contract for review, please see the [Vendor and Contract Approval Workflow](/handbook/finance/procure-to-pay/#vendor-and-contract-approval-workflow).
Once the contract is completed it should be uploaded by the requestor into our contract management database tool [ContractWorks](/handbook/legal/vendor-contract-filing-process/).

## Legal Engagement for Vendor Contracts

Procurement is responsible for reviewing vendor contracts and will adhere to legal playbook.
Procurement will escalate contracts to the legal team if support and/or legal expertise is required on a case by case basis.

Legal escalation will occur in the event:

1.  A request exceeds $100K USD;
2.  DPIA is required for the procurement of goods / services;
3.  Purchase impacts or relates to Intellectual Property ownership right(s) (i.e., joint development agreement for product(s); or
4.  Consultant, Contractor and/or Sub-Contractor requests

In the event Legal escalation is required:

1.  Legal will be added as an approver to the Vendor Contract Approval Template; and
2.  Once reviewed and approved the contract(s) will be updated with a legal / contract manager approval stamp.

## Capacity & Back Log

In the event the procurement team is OOO (as highlighted in PTO calendar or Slack), and the matter is time sensitive, requestor should
contact #legal channel in Slack and provide:

1.  Link to Vendor Contract Approval Issue; and
2.  Reason for escalation, with timeline for requirement(s)

Legal, once engaged with assign a team member to the Vendor Contract Approval Issue.

In addition, in the event of overflow of requests, procurement will engage legal to support contract reviews via the #legal slack channel.

### 4. Vendor Onboarding

As new (and existing) vendors will be supporting GitLab, we are implementing new tools such as Tipalti for onboarding and a PO Module for all payments.
Vendors will be required to create an account within Tipalti in order to receive payment.

### 5. Vendor Performance Issues

If there are performance and/or quality issues from an existing third-party vendor, procurement can support the resoluation and/or re-evaluation of a replacement vendor.
Please log an issue and assign `a.hansen` for next steps.

### 6. All Other Procurement Requests

If you have a question please ask via the #procurement slack channel.

# Contract Templates

- [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)
- [Data Processing Addendum](https://drive.google.com/file/d/1Of9hxDYpKDW4t9VF2Zay-lByZrxGx9DY/view?ts=5cdda534)
- [SaaS Addendum](https://drive.google.com/file/d/1NwaYid6qIJk9YscaRoY-uY5bEGsN1uu2/view?usp=sharing)

# General Topics and FAQs

## 1. What is the main objectives of Procurement?

At GitLab Procurement operates with three main goals
1.  Cost Savings
2.  Centralize Spending
3.  Compliance

More FAQ's coming soon! Please submit relevant questions to @a.hansen.

# Important Pages Related to Procurement

* [Compliance](/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [General Guidelines](/handbook/general-guidelines/) - general company guidelines
* [Terms](/terms/) - legal terms governing the use of GitLab's website, products, and services
* [Privacy Policy](/privacy/) - GitLab's policy for how it handles personal information
* [Trademark](/handbook/marketing/corporate-marketing/#gitlab-trademark--logo-guidelines) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents

## Related Processes

- [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)