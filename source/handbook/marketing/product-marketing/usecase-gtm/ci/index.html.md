---
layout: markdown_page
title: "Usecase: Continuous Integration"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Continuous Integration

The Continuous Integration (CI) usecase is a staple of modern software development in the digital age. It's unlikely that you hear the word "DevOps" without a reference to "Continuous Integration and Continuous Delivery" (CI/CD) soon after. In the most basic sense, the CI part of the equation enables development teams to automate building and testing their code.

When practicing CI, teams collaborate on projects by using a shared repository to store, modify and track frequent changes to their codebase. Developers check in, or integrate, their code into the repository multiple times a day and rely on automated tests to run in the background. These automated tests verify the changes by checking for potential bugs and security vulnerabilities, as well as performance and code quality degradations. Running tests as early in the software development lifecycle as possible is advantageous in order to detect problems before they intensify.

CI makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. They get earlier feedback on their code in order to iterate and improve it quickly increasing the overall pace of innovation. Studies done by DevOps Research and Assement (DORA) have shown that [robust DevOps practices lead to improved business outcomes](https://cloud.google.com/devops/state-of-devops/). Out of the "DORA 4" metrics, 3 of them can be improved by using CI: 
- **Deployment frequency:** Automated build and test is a pre-requisite to automated deploy.
- **Time to restore service:** Automated pipelines enable fixes to be deployed to production faster reducing Mean Time to Resolution (MTTR)
- **Change failure rate:** Early automated testing greatly reduced the number of defects that make their way out to production.

[GitLab CI](/product/continuous-integration/) comes built-in to GitLab's complete DevOps platform delivered as a single application. There's no need to cobble together multiple tools and users get a seamless experience out-of-the-box.

<!--
facts about approaching 100mil software developers...
-->

## Personas

### User Persona

The typical **user personas** for this usecase are the Developer, Development team lead, and DevOps engineer.

#### Software Developer [Sacha](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)

Software developers have expertise in all sorts of development tools and programming languages, making them an extremely valuable resource in the DevOps space. They take advantage of GitLab SCM and GitLab CI together to work fast and consistently deliver high quality code.

- Developers are problem solvers, critical thinkers, and love to learn. They work best on planned tasks and want to spend a majority of their time writing code that ends up being delivered to customers in the form of lovable features.

#### Development Team Lead [Delaney](/marketing/product-marketing/roles-personas/#delaney-development-team-lead)

Development team leads care about their team's productivity and ability to deliver planned work on time. Leveraging GitLab CI helps maximize their team's productivity and minimize disruptions to planned tasks.

- Team Leads need to understand their team's capacity to assign upcoming tasks, as well as help resolve any blockers by assigning to right resources to assist.

#### DevOps Engineer [Devon](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)

DevOps Engineers have a deep understanding of their organization's SDLC and provide support for infrastructure, environments, and integrations. GitLab CI makes their life easier by providing a single place to run automated tests and verify code changes integrated back into the SCM by development teams.

- DevOps Engineers directly support the development teams and prefer to work proactively instead of reactively. They split their time between coding to implement features and bug fixes, and helping developers build, test, and release code.


### Buyer Personas

CI purchasing typically does not require executive involvement. It is usually acquired and installed via our freemium offering without procurement or IT's approval. This process is commonly known as shadow IT. When the upgrade is required the [VP of IT](/handbook/marketing/product-marketing/roles-personas/#buyer-personas) is the most frequent decision-maker. The influence of the [VP Application Development](/handbook/marketing/product-marketing/roles-personas/#buyer-personas) is notable too.


### [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

## Analyst Coverage

- [Forrester Wave for Cloud-Native CI Tools](/analysts/forrester-cloudci19/)
- [Forrester Continuous Integration Tools](/analysts/forrester-ci/)

### [AR Plan](./ar-plan/)

The AR Plan provides key details into how we intend to engage with the analyst community.

## UseCase Capabilities

tbd

## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **Leading SCM and CI in one application** | PLACEHOLDER | **-** PLACEHOLDER |
| **Built in security and compliance** | PLACEHOLDER | **-** PLACEHOlDER |
| **Rapid innovation** | PLACEHOLDER | **-** PLACEHOLDER |


## Competitive Comparison
Amongst the many competitors in the DevOps space, [Jenkins](/devops-tools/jenkins-vs-gitlab.html) and [CircleCI](/devops-tools/circle-ci-vs-gitlab.html) are the closest competitors offering continuous integration capabilities.

## Proof Points - customers

### Quotes and reviews

#### G2 Review

> "Gitlab CI is a revolutionary CI, no need to connect multiple services together (ex. Github & Travis CI). The flexibility of the different types of CI runners (SSH, docker, etc) allows users to customize the experience to their needs. Another advantage of Gitlab over its competitors is the unlimited free private repositories as well as the free CI, you do need to wait for a public runner to be available which makes it hard to debug but its free."
>
> **- Zachary B., Software Development Specialist**

### Blogs

#### Jaguar Land Rover

**[How Jaguar Land Rover embraced CI to speed up their software lifecycle](/blog/2018/07/23/chris-hill-devops-enterprise-summit-talk/)**
> Our feedback loops were 4-6 weeks. Could you imagine writing code today and six weeks from now being told whether or not it works or is broken? I don't remember the shirt that I wore yesterday, let alone what I had for breakfast this morning, let alone what I wrote six weeks ago, and chances are I've been working on features for the last six weeks, and for me to try to unpick what I was thinking at that point could be a huge context-switch penalty.

#### Ticketmaster

**[GitLab CI supports Ticketmaster's ramp up to weekly mobile releases](/blog/2017/06/07/continous-integration-ticketmaster/)**
> With the benefit of faster cycle time, and faster releases, we have seen other benefits. Since each release has a smaller change set, our crash-free rates and store ratings have improved. We have less time waiting for build and spend more time improving the quality of our products. Our fans are getting features into their hands more quickly and benefit from a higher-quality and a consistently improving product. The CI analytics available on GitLab are an additional scoreboard for our team to optimize and improve into the future.

### Case Studies

#### Goldman Sachs

**[Goldman Sachs improves from 1 CI feature branch build every two weeks to over a thousand per day](/customers/goldman-sachs/)**
> GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!

#### CERN (European Organization for Nuclear Research)

**[CERN, the European-based particle physics laboratory, uses GitLab for more than 12,000 users and 120,000 CI jobs a month](/customers/cern/)**
> The entire infrastructure, with continuous integration and container support, makes it possible to have new scientific results based on code that was developed once before. Having an easily accessible record on how the original code was developed makes that much easier. This is why we are using GitLab CI, pipelines and starting with the Auto DevOps tools.

### References to help you close

See an exhaustive list of publicly available references and [Proof Points](/handbook/sales/command-of-the-message/proof-points.html#gitlab-proof-points) based on our [Command of the Message](/handbook/sales/command-of-the-message/).

## Resources

### Presentations
* [Why CI/CD?](https://docs.google.com/presentation/d/1OGgk2Tcxbpl7DJaIOzCX4Vqg3dlwfELC3u2jEeCBbDk)

### Continuous Integration Videos
* [CI/CD with GitLab](https://youtu.be/1iXFbchozdY)
* [GitLab for complex CI/CD: Robust, visible pipelines](https://youtu.be/qy8A7Vp_7_8)

### Integrations Demo Videos
* [Migrating from Jenkins to GitLab](https://youtu.be/RlEVGOpYF5Y)
* [Using GitLab CI/CD pipelines with GitHub repositories](https://youtu.be/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [Live Demo: GitLab CI Deep Dive](https://youtu.be/pBe4t1CD8Fc)
