---
layout: markdown_page
title: "Asana"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Gaps
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Asana is used for collaborative project management and tracking.  It can integrate emails, files, tickets, and more offering over 100 integrations. GitLab is a complete DevOps platform, delivered as a single application, providing not only project management, but also planning, CI/CD, packaging, verification, release, and monitoring, all automatically looping back their status and data from across the lifecycle into the issues being tracked.
## Weakness
Asana is less powerful than GitLab Issues within the issue descriptions themselves... e.g. no deep linking to comments, no automatic cross-linking between issues when they are mentioned, etc.
## Resources
* [Asana website](https://asana.com/)
* [Asana Wikipedia](https://en.wikipedia.org/wiki/Asana_(software))
