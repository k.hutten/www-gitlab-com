---
layout: markdown_page
title: "Theme Direction - CI/CD Onboarding"
---

- TOC
{:toc}

## CI/CD Onboarding

TBD

Contact [Thao Yeager](https://gitlab.com/thaoyeager), the PM for this theme.

- [Overall Vision](/direction/cicd)
- Documentation: TBD

## What's Next & Why

TBD

## Maturity Plan

TBD

## Competitive Landscape

TBD

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

TBD

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

TBD
