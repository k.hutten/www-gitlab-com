---
layout: markdown_page
title: "Category Direction - GitLab Handbook"
---

- TOC
{:toc}

## GitLab Handbook

| Stage | Maturity |
| --- | --- |
| Create | Complete |

## Overview

<!--
A good description of what your category is.
If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it.
Please include usecases, personas, and user journeys into this section.
-->

The GitLab Handbook is an incredible tool for documenting GitLab processes and is used as the single source of truth for many things inside the company. It's also an incredible recruiting tool as many candidates browse through the handbook before applying.

Currently the GitLab Handbook project serves our marketing home page, our blog, and our company handbook.

### Target Audience

<!--
An overview of the personas involved in this category.
An overview of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.
-->

**GitLab Team Members:** As the GitLab team grows, it will be important that every team member can contribute to our handbook, which is a
static site. Therefore, the features delivered out of this group will serve all GitLab team members, both technical and non-technical.

**Leadership:** As companies move to a more distributed way of working, processes and documentation becomes more important. The features
delivered out of this group will also be packaged in a way that will help leadership at other organizations create and manage static sites.

## Where we are Headed

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this category once your strategy is at least minimally realized.
-->

The GitLab handbook should be deployed within seconds of changes happening. Currently it is difficult to predict when a specific change will make its way onto the handbook due to the testing, build, and deployment process taking between 10 and 45 min.

A world class handbook should have:
* Incredible content
* An amazing editing experience, which will be handled out of the Static Site Editor category
* Deployments within seconds of making a change to a single page

### What's Next & Why

<!--
This is almost always sourced from the following sections, which describe top priorities for a few stakeholders.
This section must provide a link to an issue or [epic](/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.
-->

- **Under Investigation:** [Speed up marketing site builds and deployments, including the handbook and the blog](https://gitlab.com/groups/gitlab-com/-/epics/191)
- **Planned:** [Measure handbook metrics](https://gitlab.com/groups/gitlab-com/-/epics/203)

### What is Not Planned Right Now

<!--
Often it's just as important to talk about what you're not doing as it is to discuss what you are.
This section should include items that people might hope or think we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should in fact do.
We should limit this to a few items that are at a high enough level so someone with not a lot of detailed information about the product can understand the reasoning.
-->

Migration to another static site framework or wiki product.

### Maturity Plan

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

Currently, the GitLab Handbook category is a *non-marketing category* which means its maturity does not get tracked. However, for the sake of measuring improvement, the GitLab Handbook is marked as `Complete` with intentions of moving it to `Lovable`.

## Top internal customer issue(s)

<!--
These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding) the product.
-->

TBD
