---
layout: markdown_page
title: "Category Direction - Analysis"
---

- TOC
{:toc}

## 🔍 Analysis

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

* [Overall Vision](/direction/telemetry)
* [Roadmap]()
* [Maturity: Minimal](/product/categories/maturity)
* [Documentation]()
* [All Epics]()

The Analysis category covers GitLab's internal needs for an Analysis tool that serves the Product Department.

Please reach out to  Tim Hey, Interim Product Manager for Telemetry ([E-Mail](mailto:they@gitlab.com) if you'd like to provide feedback or ask
questions about what's coming.

## 🔭 Vision  

TBD

## 🎭 Target audience  
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

### Parker (Product Manager) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)

* 🙂 **Minimal** -
* 😊 **Viable** -  
* 😁 **Complete** -
* 😍 **Lovable** -

### Dana (Data Analyst) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#dana-data-analyst)

* 🙂 **Minimal** -
* 😊 **Viable** -  
* 😁 **Complete** -
* 😍 **Lovable** -

For more information on how we use personas and roles at GitLab, please [click here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

## 🚀 What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->
#### 🔥 Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ |[Pendo Implementation for GitLab hosted services](https://gitlab.com/groups/gitlab-org/-/epics/1706)|  Pendo.io is an wildly popular, industry standard data collection and analysis tool widely used by many organisations to gain insights into how their users are using their products. GitLab has decided to subscribe to and implement Pendo whilst we improve our in-house data collection and analysis options and develop the overall vision for the Telemetry group.  |

#### 🎉 Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ | [Product Team Dashboards](https://gitlab.com/groups/gitlab-org/-/epics/1325) | Parallel to improving [SMAU/MAU v2.0.](https://gitlab.com/groups/gitlab-org/-/epics/1440), it's important that we roll out the process defined by the Telemetry working group to all of the other stages so that each Product Manager has visibility into how users are using their stage and stage categories. |

<!--  ## 🏅 Competitive landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!-- ## Analyst landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ## Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- ## 🎢 Top user issues
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!-- ## 🦊 Top internal customer issues/epics  
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

<!--  ## Top Vision Item(s)
What's the most important thing to move your vision forward?-->
